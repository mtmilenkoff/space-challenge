package extensions

import spacechallenge.classes.Rocket


//Adds protective shield to rocket ensuring safe launch and lad
fun Rocket.activeEnergyShield(){
    this.crash = 0
    this.explosion = 0
}

//Gets space left on rocket
fun Rocket.spaceLeft(): Double{
    return this.maxWeight-this.weight
}
