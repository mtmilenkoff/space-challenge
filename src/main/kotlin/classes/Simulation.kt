package spacechallenge.classes

import extensions.activeEnergyShield
import extensions.spaceLeft
import java.io.File

class Simulation {

    //Method to get the items to load from a txt file
    fun loadItems(file: File): ArrayList<Item> {
        return try{
            //Read the file by line giving you a list of strings, then we split each string on the "=". Making the end result a list of lists with the format [[name, weight],[name, weight]...]
            val stringList: List<List<String>> = file.readLines().map { e -> e.split("=") }

            //Turn the previous list, to a list of items and return it
            stringList.map { e -> Item(e[0], e[1].toDouble()) } as ArrayList<Item>
        }catch(e: Error){
            println("Error trying to read file\n ${e.message}")
            ArrayList()
        }
    }


    //Method to load the items on rocket type U1
    fun loadU1(itemInput: ArrayList<Item>): ArrayList<Rocket>{

        //We create a new list to not change the input
        var items: ArrayList<Item> = ArrayList(itemInput)
        val rockets: ArrayList<Rocket> = ArrayList()

        //Order the items by weight, the heaviest first
        items.sortByDescending { e -> e.weight }

        try{
            loadRocket(1, items, rockets)
        }catch(e: Error){
            println(e.message)
        }

        return rockets
    }


    //Same method as loadU1 but with using U2 rockets
    fun loadU2(itemInput: ArrayList<Item>): ArrayList<Rocket>{

        var items: ArrayList<Item> = ArrayList(itemInput)
        val rockets: ArrayList<Rocket> = ArrayList()
        items.sortByDescending { e -> e.weight }

        try{
            loadRocket(2, items, rockets)
        }catch(e: Error){
            println(e.message)
        }

        return rockets
    }

    //Run simulation
    fun runSimulation(rockets: ArrayList<Rocket>): Int{
        var totalCost: Int = 0

        //We check if the rockets land and launch successfully, while they don't we keep launching and adding the cost. Every third we add a shield
        rockets.forEachIndexed() { i, rocket ->
            if(i%3==0){ rocket.activeEnergyShield() }

            do{
                totalCost+=rocket.cost
            }while (!rocket.launch() || !rocket.land()) }

        return totalCost
    }

    private fun loadRocket(type: Int, items:ArrayList<Item>, rockets:ArrayList<Rocket>){

        while(true){
            if(items.isEmpty()){ break }

            //Each loop we create a new rocket of the arguments type, and we restart the iterator for the updated item list
            when(type){
                1 -> rockets.add(U1())
                2 -> rockets.add(U2())
                else -> throw error("Error: Rocket type not found")
            }
            var itemIterator = items.iterator()


            //If one of the items is too heavy to be carried we return an empty list, printing which item is causing the problem
            if(items[0].weight>rockets.last().spaceLeft()){
                throw error("Error: One of the items  ${items[0]}  is too big to be carried, try with other type of rocket or remove this item")
            }

            //Load each item to the rocket removing it from the list, until the rocket is filled or until there are no items left
            while(itemIterator.hasNext()){
                var item = itemIterator.next()
                if(rockets.last().canCarry(item)) {
                    rockets.last().carry(item)
                    itemIterator.remove()
                }
            }
        }

    }



}