package spacechallenge.classes

interface Spaceship{
    fun launch(): Boolean
    fun land(): Boolean
    fun canCarry(item: Item): Boolean
    fun carry (item: Item)
}