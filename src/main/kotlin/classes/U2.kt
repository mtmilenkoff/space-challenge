package spacechallenge.classes

import kotlin.random.Random

class U2(): Rocket(120,18000.0,29000.0,4,8) {


    //Return TRUE if rocket launched successfully, FALSE if it explodes on launch
    override fun launch(): Boolean {
        return Random.nextDouble()>(explosion*(weight/maxWeight))/100
    }

    //Return TRUE if rocket lands successfully, FALSE if it crashes on land
    override fun land(): Boolean {
        return Random.nextDouble()>(crash*(weight/maxWeight))/100
    }
}