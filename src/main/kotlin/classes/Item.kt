package spacechallenge.classes

data class Item(var name: String, var weight: Double)