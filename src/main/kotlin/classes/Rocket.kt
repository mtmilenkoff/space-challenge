package spacechallenge.classes

open abstract class Rocket(
    val cost: Int,
    var weight: Double,
    val maxWeight: Double,
    var explosion: Int,
    var crash: Int): Spaceship {

    override fun launch(): Boolean {return true}

    override fun land(): Boolean {return true}

    override fun canCarry(item: Item): Boolean {
        return maxWeight>=weight+item.weight
    }

    override fun carry(item: Item) {
        weight += item.weight
    }
}