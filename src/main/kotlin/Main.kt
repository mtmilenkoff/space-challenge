package spacechallenge

import spacechallenge.classes.Simulation
import java.io.File

fun main(args: Array<String>){
    val simul = Simulation()

    var items1 = simul.loadItems(File("src/main/kotlin/files/Phase-1.txt"))
    var items2 = simul.loadItems(File("src/main/kotlin/files/Phase-2.txt"))

    var rockets1 = simul.loadU1(items1)
    var rockets2 = simul.loadU1(items2)

    var cost1 = simul.runSimulation(rockets1)
    var cost2 = simul.runSimulation(rockets2)
    println("The cost using rockets U1 is: ${cost1+cost2}")

    rockets1 = simul.loadU2(items1)
    rockets2 = simul.loadU2(items2)

    cost1 = simul.runSimulation(rockets1)
    cost2 = simul.runSimulation(rockets2)
    println("The cost using rockets U2 is: ${cost1+cost2}")

    }

